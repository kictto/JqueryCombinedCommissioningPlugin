# Jquery N级联调插件

# JqueryCombinedCommissioningPlugin

jquery插件，实现多级联调，无论是二级，三级，还是更多级联调，以及初始值的设置，都可以直接在插件初始化的时候实现，当然了，设计方法，参考了x-editable。不过比x-editable 轻量化了

## 使用方法：

### Step1:
```
<script type="text/javascript" src="js/jquery.js" ></script>
<script type="text/javascript" src="js/jquery.select.js" ></script>
```
首先引入jquery和select插件

### Step2:
```
<select id="sel1" style="width: 300px;"></select> &nbsp;&nbsp;&nbsp; 
<select id="sel2" style="width: 300px;"></select> &nbsp;&nbsp;&nbsp; 
<select id="sel3" style="width: 300px;"></select>
```
添加三个select，三级联调

### Step3:
```
	<script>
		var tempId={
			p_cid:1,
		};
		$('#sel1').select({
			value:1,
			source:'http://127.0.0.1/dt/Home/User/getCountry',
			success:function(data){
				tempId.p_cid=data;
				$('#sel2').select("option","source",'http://127.0.0.1/dt/Home/User/getProvince?p_cid='+data);
			}
		});
		$('#sel2').select({
			value:1,
			source:'http://127.0.0.1/dt/Home/User/getProvince',
			success:function(data){
				$('#sel3').select("option","source",'http://127.0.0.1/dt/Home/User/getCity?p_cid='+tempId.p_cid+"&p_pid="+data);
			}
		});
		$('#sel3').select({
			value:1,
			source:'http://127.0.0.1/dt/Home/User/getCity',
			success:function(data){
				console.log(data);
			}
		});
	</script>
```
初始化这三个select

### Step4：
然后就没有然后拉~~~

PS：
需要提供的数据格式：
```
<script>
var souce=[{value:1,text:"选项1"},{value:2,text:"选项2"}];
</script>
```

配置中的source，可以使用url，也可以使用构造好的数组对象，需要注意的是，
url返回的，必须是json格式的如上的数据。

### 2015-12-28 更新
有时候，我们会有需要，只需要对几个类似的下拉框设置初值，然而一般我们是配合后端程序或者用模板引擎才能够实现在指定的option上加上selected属性，
对于这样的情况，本插件支持了链式调用，也支持了多选择器的绑定，本次更新，也为了支持多选择器的动态绑定而做了小调整，保证在动态绑定的时候，不会破坏已经生成、选中过的下拉框
使用方法：
首先定义一个
```
var finaSelSource=[ {
	text : '种子轮',
	value : '种子轮'
}, {
	text : '天使轮',
	value : '天使轮'
}, {
	text : 'A轮',
	value : 'A轮'
}, {
	text : 'B轮',
	value : 'B轮'
}, {
	text : 'C轮',
	value : 'C轮'
}, {
	text : 'D轮',
	value : 'D轮'
}, {
	text : 'E轮',
	value : 'E轮'
}, {
	text : 'IPO',
	value : 'IPO'
} ];
```
然后给所有class里面有selL的下拉框绑上我们的插件：

```
<script>
	$('.selL').select({
		source : finaSelSource
	});
</script>
```
然而这还不够，我们需要动态添加这样的下拉框怎么办呢：
在动态添加页面元素的地方：
```
function add(){
    $('button','form#7-com-money').on('click',function(){
        //此处是动态添加元素，自己的代码
        $(this).siblings('ul#y-pub-financ').append('<select class="finanum selL"></select>');
        //这里是将插件动态绑定到select下拉框上去
        $('.selL').select({
	    source : finaSelSource
	});
        
    });
}
```

最后，祝使用愉快。新手，希望大家多多支持，存在的问题，也希望大家能够指出来，这是第一版，也是第一次写jquery插件，大家轻点儿喷 :-)